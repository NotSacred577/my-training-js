import random

def question_generator(min_num, max_num):

    random_number1 = random.randint(min_num,max_num)
    random_number2 = random.randint(min_num,max_num)
    
    question = f"{random_number1} x {random_number2} = ?"
    print(question)
    answer = int(input("Answer: "))
    
    if random_number1 * random_number2 == answer:
        return "Answer Correct"
    else:
        return f"Answer Incorrect, It was {random_number1 * random_number2}"


# Enter the range to generate a random question
# Answer the questions asked

print(question_generator(1,12))
