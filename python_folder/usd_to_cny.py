def usdcny():
    usd = int(input("USD:"))
    cny = usd*6.75
    converted_cny = "{:.2f}".format(cny)
    answer = f"{converted_cny} Chinese Yuan"
    return answer
 
print(usdcny())