let letters = [
  "a",
  "b",
  "c",
  "d",
  "e",
  "f",
  "g",
  "h",
  "i",
  "j",
  "k",
  "l",
  "m",
  "n",
  "o",
  "p",
  "q",
  "r",
  "s",
  "t",
  "u",
  "v",
  "w",
  "x",
  "y",
  "z",
];

console.log("===========================");
console.log(`LETTERS ${letters}`);
console.log(`INDEX OF J ${letters.indexOf("j")}`); // expected 9
letters[letters.indexOf("j")] = letters[letters.indexOf("z")];
console.log(`NEW ARRAY ${letters}`);
console.log("===========================");
