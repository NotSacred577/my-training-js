// Create a function that accepts an array of numbers as a parameter
// and returns the amount of even numbers that exist in that array.
//
// Examples:
// arrayDivision([1,2,3,4,5,6]) // 3
// arrayDivision([1,3,5,7,9,21]) // 0
// arrayDivision([2,4,6,8,10]) // 5

function arrayDivision(array) {
  let counter = 0;
  for (let index = 0; index < array.length; index++) {
    if (array[index] % 2 == 0) {
      counter = counter + 1;
    }
  }
  return counter;
}

console.log(arrayDivision([1, 2, 3, 4, 5, 6])); // 3
console.log(arrayDivision([1, 3, 5, 7, 9, 21])); // 0
console.log(arrayDivision([2, 4, 6, 8, 10])); // 5
console.log(arrayDivision([1, 3, 4, 8, 8, 9, 12, 14, 27, 36])); //6
