// Enter the sequence
// Enter the term of the number in the sequence you want to see
// formula will be: An+c = number
// example. You have the sequence 2,3,4. You want to see the term 6. The formula used would be
// 1n+1. So if you want to see the 100 term it would be: 101

function sequenceGenerator(arr, nTerm){
    let expression = 0;
    let termToTermRule = 0;
    termToTermRule = arr[1] - arr[0]
    if(arr[0] == 0){
        termToTermRule = arr[2] - arr[1]
    }
  if(termToTermRule * nTerm == arr[0]){
      return termToTermRule * nTerm
  }
  else{
      if(arr[0] - termToTermRule * nTerm >= 0){
        expression = arr[nTerm] - termToTermRule
      }
  }

  return termToTermRule*nTerm+expression
}

console.log(sequenceGenerator([2, 3, 4], ))