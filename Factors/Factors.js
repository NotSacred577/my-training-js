function sumMultiples(number) {
  let storage = 0;
  for (let index = 0; index < number; index++) {
    if (index % 3 == 0 || index % 5 == 0) {
      storage = storage + index;
    }
  }

  return storage;
}

console.log(sumMultiples(11));
