// Printer Errors
// Examples:
//   	s="aaabbbbhaijjjm"
//	  printer_error(s) => "0/14"
//
//	  s="aaaxbbbbyyhwawiwjjjwwm"
//	  printer_error(s) => "8/22"
// LINK: https://www.codewars.com/kata/56541980fa08ab47a0000040

function printerError(code) {
  let counter = 0;
  for (let index = 0; index < code.length; index++) {
    if (code[index] > "m" || code[index] < "a") {
      counter++;
    }
  }
  return `${counter}/${code.length}`;
}
console.log(
  printerError("aaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbmmmmmmmmmmmmmmmmmmmxyz")
);
