function isValidIP(str) {
  let reStr = "(\\d|[1-9]\\d|1\\d{2}|2[0-4][0-9]|25[0-5])";
  let re = new RegExp(`^${reStr}\\.${reStr}\\.${reStr}\\.${reStr}$`);
  return re.test(str);
}
console.log(isValidIP("192.168.25.249"));
