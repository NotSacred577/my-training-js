function wordCount(str) {
    let counter = 0;
    let excludedWordsDetected = 0;
    let b = str.split(" ")
    const bMapped = b.map(b => {
        if (b == "I’d") {
            let  c = ["I", "’d"]
            return c;
        }
        if (b == "I'd") {
            let  c = ["I", "'d"]
            return c;
        }
        if (b == "slow-moving") {
            let c = ["slow", "moving"]
            return c; 
        }
        return b;
    })
    b = bMapped.flat()
    let a = ["a", "the", "upon", "on", "at", "of", "in", "as"]
    for (let i = 0; i < b.length; i++) {
        for (let ind = 0; ind < a.length; ind++) {
            if (a[ind] == b[i]) {
                excludedWordsDetected++;
                counter = b.length - excludedWordsDetected
            }
        }
    }
    if (counter == 0) {
        counter = b.length
    }
    return counter;
}

console.log(wordCount("I’d been using my sphere as a stool. I traced counterclockwise circles on it with my fingertips and it shrank until I could palm it. My bolt had shifted while I’d been sitting. I pulled it up and yanked the pleats straight as I careered around tables, chairs, globes, and slow-moving fraas. I passed under a stone arch into the Scriptorium. The place smelled richly of ink. Maybe it was because an ancient fraa and his two fids were copying out books there. But I wondered how long it would take to stop smelling that way if no one ever used it at all; a lot of ink had been spent there, and the wet smell of it must be deep into everything."))