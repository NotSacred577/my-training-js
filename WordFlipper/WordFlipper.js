function wordFlipper(word) {
  let wordOutput = "";
  for (let index = 0; index < word.length; index++) {
    let reverseIndex = word.length - 1 - index;
    let storage = word[reverseIndex];
    wordOutput = wordOutput + storage;
  }
  return wordOutput;
}
function wordFlipper2(word) {
  let wordOutput = "";
  let indexA = word.length - 1;
  for (let index = indexA; index >= 0; index--) {
    let storage = word[index];
    wordOutput = wordOutput + storage;
  }
  return wordOutput;
}
console.log(wordFlipper2(""));
