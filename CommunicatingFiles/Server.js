let contacts = [
  {
  name: "Alexander Barrelier",
  age: "12",
  sex: "M",
  id: "1"
  },
  {
    name: "Alexander Barrelier",
    age: "41",
    sex: "M",
    id: "2"
  },
  {
    name: "Florie Lenoel",
    age: "31",
    sex: "F",
    id: "3"
  },
  {
    name: "Chloe Barrelier",
    age: "3",
    sex: "F",
    id: "4"
  }
  
]
const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 418;
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify(contacts));
});

server.listen(port, hostname, () => {
  console.log(`El servidor se está ejecutando en http://${hostname}:${port}/`);
});
