const http = require('http')
const options = {
  hostname: '127.0.0.1',
  port: 3000,
  path: '/',
  method: 'GET'
}

const req = http.request(options, res => {
  console.log(`statusCode: ${res.statusCode}`)

  res.on('data', d => {
    let parsedResponse = JSON.parse(d)
      process.stdout.write(parsedResponse[0].name)
  })
})

req.on('error', error => {
  console.error(error)
})

req.end()
