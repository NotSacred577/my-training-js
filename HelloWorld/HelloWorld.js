//enter values here
let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

let storage = 0;

numbers.forEach((number) => {
  storage = storage + number;
});

//colors
HEADER = "\033[95m";
OKBLUE = "\033[94m";
OKCYAN = "\033[96m";
OKGREEN = "\033[92m";
WARNING = "\033[93m";
FAIL = "\033[91m";
ENDC = "\033[0m";
BOLD = "\033[1m";
UNDERLINE = "\033[4m";

//answer
let answer = storage;
console.log(`the answer is ${WARNING}${answer}${ENDC}`);
