function getMiddle(str) {
  let finalStr = "";
  let indexButDif = str.length / 2;
  let indexButSec = 0;

  if (str.length % 2 == 1) {
    indexButDif = indexButDif - 0.5;
    finalStr = str[indexButDif];
  } else {
    indexButSec = indexButDif - 1;
    indexButDif = indexButDif + 1;
    finalStr = str.substring(indexButSec, indexButDif);
  }

  return finalStr;
}
console.log(getMiddle("test")); // es
console.log(getMiddle("testing")); // t
console.log(getMiddle("middle")); // dd
console.log(getMiddle("A")); // A
