//Functions

let numbers = [1, 2, 3];
let a = 4;
let b = 2;

function sum(numbers) {
  let storage = 0;
  numbers.forEach((number) => {
    storage = storage + number;
  });
  return storage;
}

function div(a, b) {
  let storage = a / b;
  return storage;
}

function mult(a, b) {
  let storage = a * b;
  return storage;
}

function sub(a, b) {
  let storage = a - b;
  return storage;
}

//enter question here (remove "//" of the command "console.log" to use the operation.Remember to separate all by commas.)

//addition (enter values in the "[]" separated by commas)
//console.log(sum([]));

// division
//console.log(div());

//Multiplication
//console.log(mult());

//substraction
//console.log(sub());
