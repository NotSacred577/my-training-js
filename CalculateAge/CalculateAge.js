function toPositive(negativeNumber) {
  output = -negativeNumber;

  return output;
}
function calculateAge(bornYear, nowYear) {
  let output = nowYear - bornYear;
  if (output == -1) {
    output = toPositive(output);
    return `You will be born in ${output} year.`;
  }
  if (output == 1) {
    return `You are ${output} year old.`;
  }
  if (output < 0) {
    output = toPositive(output);
    return `You will be born in ${output} years.`;
  }
  if (output == 0) {
    return `You were born this very year!`;
  }
  return `You are ${output} years old.`;
}
console.log(calculateAge(2009, 0));
