//function name is "isPalindrome"
function isPalindrome(word) {
  //The default value is true and changes to false if the letters are not equal
  let PalindromeModerator = true;
  //checks for every letter to see if they are equal
  for (index = 0; index < word.length; index++) {
    let reverseIndex = word.length - 1 - index;
    //changes the value of PalindromeModerator
    if (word[index] != word[reverseIndex]) {
      PalindromeModerator = false;
    }
  }
  return PalindromeModerator;
}
//Enter words in the ""
console.log(isPalindrome("racecar"));
