// Science Work needed cheat by Alexander Barrelier
// Instructions :
// enter the values in this fomrat: 8 kg <- is correct. 8kg <- is incorrect
// enter mass first then distance. Remember to enter them inside the "".
function workNeeded(mass, distance) {
    // variables
    let work = 0;
    let gravity = 9.807;
    // mass procesing
    let procesableMass = mass.split(" ")
    let processedMass = 0;
    if (procesableMass[1] == "g") {
        processedMass = procesableMass[0] / 1000
    }
    if (procesableMass[1] == "mg") {
        processedMass = procesableMass[0] / 1000000
    }
    if (procesableMass[1] == "kg") {
        processedMass = procesableMass[0]
    }
    // distance procesing
    let procesableDistance = distance.split(" ")
    let processedDistance = 0;
    if (procesableDistance[1] == "km") {
        processedDistance = procesableDistance[0] * 1000
    }
    if (procesableDistance[1] == "m") {
        processedDistance = procesableDistance[0]
    }
    // geting value that needs to be worked with to convert it to newtons
    work = processedMass * gravity * processedDistance;
    //transforming to newton
    return `${work}Nm`;
}
console.log(workNeeded("8 kg", "100 m"))
