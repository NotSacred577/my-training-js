function fallingForce(weight) {
    let gravity = 9.807;
    let procesableWeight = weight.split(" ");
    let processedWeight = 0;
    if (procesableWeight[1] == "mg") {
        processedWeight = procesableWeight[0] / 1000;
    }
    if (procesableWeight[1] == "kg") {
        processedWeight = procesableWeight[0] * 1000;
    }
    if (procesableWeight[1] == "g") {
        processedWeight = procesableWeight[0]
    }
    // force is equal to F = MG (mass * gravity)
    let force = processedWeight * gravity
    return `${force}n`
}
console.log(fallingForce("8000 g"))