// DomainNameFromURL takes the url and returns the domain name
//
function domainName(URL) {
    let reg1 = /www\.(?<domainName>\w+)\./
    let reg2 = /\w+:\/\/www\.(?<domainName>\w+)\./
    let reg3 = /\w+:\/\/(?<domainName>\w+)\./
    let reg4 = /(?<domainName>\w+)\./
    let domain = "";
    if (reg1.test(URL)) {
        domain = URL.match(reg1).groups.domainName
    } else if (reg2.test(URL)) {
        domain = URL.match(reg2).groups.domainName
    } else if (reg3.test(URL)) {
        domain = URL.match(reg3).groups.domainName
    }
    else if (reg4.test(URL)) {
        domain = URL.match(reg4).groups.domainName
    }
    return domain;
}

// tests
console.log(domainName("http://google.com"))
// expected: google

console.log(domainName("http://google.co.jp"))
// expected: google

console.log(domainName("www.xakep.ru"))
// expected: xakep

console.log(domainName("https://youtube.com"))
// expected: youtube

console.log(domainName("http://github.com/carbonfive/raygun"))
// expected: github

console.log(domainName("http://www.zombie-bites.com"))
// expected: zombie-bites

console.log(domainName("https://www.cnet.com"))
// expected: cnet

console.log(domainName("tabnine.com"))