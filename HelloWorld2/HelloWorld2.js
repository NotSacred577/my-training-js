function sumAll(numbers) {
  let storage = 0;
  numbers.forEach((number) => {
    storage = storage + number;
  });
  return storage;
}

let numbers = [1, 2, 3, 4, 5];

console.log(sumAll([(1, 2, 3, 4, 5)])); // 15
console.log(sumAll([1, 10, 20, 304])); // 335
console.log(sumAll([-10, -20, -30])); // -60
console.log(sumAll([445, 663, 222, 990])); //2320
console.log(sumAll(numbers)); //15
